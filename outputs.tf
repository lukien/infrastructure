output "website_url" {
  description = "Name (id) of the bucket"
  value       = module.website_s3_bucket.website_url
}
