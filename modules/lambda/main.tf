#Lambda Function which adds the users in db to sqs
resource "aws_lambda_function" "form_lambda" {
  filename      = "function.zip"
  function_name = var.functionName
  role          = aws_iam_role.iam_for_formlambda.arn
  memory_size   = 128
  handler       = "lambda_function.lambda_handler"
  depends_on = [
    aws_cloudwatch_log_group.dynamo_log,
  ]

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = filebase64sha256("function.zip")

  runtime = "python3.8"

  environment {
    variables = {
      tableName = var.tableName,
      region    = var.region,
    }
  }
  tags = var.tags
}


#Loggroup for dynamodb
resource "aws_cloudwatch_log_group" "dynamo_log" {
  name              = "/aws/lambda/${var.functionName}"
  retention_in_days = 7

  tags = var.tags
}

resource "aws_iam_role" "iam_for_formlambda" {
  name = "${terraform.workspace}-iam_for_formlambda"

  assume_role_policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [
    {
        "Action": "sts:AssumeRole",
        "Principal": {
            "Service": "lambda.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
    }
	]
}
EOF
  tags               = var.tags
}


resource "aws_iam_role_policy" "dynamo_policy" {
  name = "${terraform.workspace}-dynamo_policy"
  role = aws_iam_role.iam_for_formlambda.id

  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
            "dynamodb:putItem"
        ],
        "Resource": "${var.tableArn}"
      },
      {
          "Effect": "Allow",
          "Action": [
              "logs:CreateLogStream",
              "logs:PutLogEvents"
          ],
          "Resource": "${var.accountArn}"
      }
    ]
  }
  EOF
}



