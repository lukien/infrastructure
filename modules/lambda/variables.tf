variable "functionName" {
  description = "Name of the function."
  type        = string
}

variable "region" {
  description = "region to deploy into."
  type        = string
}

variable "tableName" {
  description = "Name of the DynamoDB."
  type        = string
}

variable "tableArn" {
  description = "arn of the DynamoDB."
  type        = string
}

variable "accountArn" {
  description = "the accountArn"
  type = string
}

variable "tags" {
  description = "Tags to set on the bucket."
  type        = map(string)
  default     = {}
}
