output "website_url" {
    value = "http://${aws_s3_bucket.website_bucket.website_endpoint}"
}


output "arn" {
    value = aws_s3_bucket.website_bucket.arn
}

output "id" {
    value = aws_s3_bucket.website_bucket.id
}