variable "identity_pool_name" {
  description = "Name of the IdentityPool."
  type = string
}

variable "iam_role_name" {
  description = "Name of the Role."
  type = string
}

variable "user_pool_client" {
  description = "Name of the Client."
  type = string
}

variable "user_pool" {
  description = "Name of the User Pool."
  type = string
}





variable "tags" {
  description = "Tags to set on the bucket."
  type = map(string)
  default = {}
}