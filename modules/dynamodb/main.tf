
resource "aws_dynamodb_table" "form_table" {
  name           = var.tableName
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "mailaddress"

  attribute {
    name = "mailaddress"
    type = "S"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }

  tags = var.tags
}