variable "tableName" {
  description = "Name of the DynamoDB."
  type = string
}

variable "tags" {
  description = "Tags to set on the bucket."
  type = map(string)
  default = {}
}