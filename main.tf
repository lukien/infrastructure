provider "aws" {
  region                  = var.region
  shared_credentials_file = var.credentialFile
  profile                 = "default"
}


#Call the identity to use the accountId
data "aws_caller_identity" "current" {}

module "website_s3_bucket" {
  source = "./modules/s3"

  bucketName = "${terraform.workspace}-${var.bucketName}"

  tags = {
    project     = var.projectTagValue
    environment = terraform.workspace
  }
}

module "cognito" {
  source = "./modules/cognito"


  identity_pool_name = "${terraform.workspace}-${var.projectTagValue}-identitiypool"
  iam_role_name      = "${terraform.workspace}-${var.projectTagValue}-unauthenticated-role"
  user_pool_client   = "${terraform.workspace}-${var.projectTagValue}-client"
  user_pool          = "${terraform.workspace}-${var.projectTagValue}-userpool"
}




module "dynamodb" {
  source = "./modules/dynamodb"

  tableName = "${terraform.workspace}-${var.tableName}"

  tags = {
    project     = var.projectTagValue
    environment = terraform.workspace
  }
}

module "lambda" {
  source = "./modules/lambda"

  functionName = "${terraform.workspace}-form-lambda"
  tableName    = "${terraform.workspace}-${var.tableName}"
  tableArn     = module.dynamodb.arn
  region       = var.region
  accountArn   = "arn:aws:logs:${var.region}:${data.aws_caller_identity.current.account_id}:*"

  tags = {
    project     = var.projectTagValue
    environment = terraform.workspace
  }
}
resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda.arn
  principal     = "s3.amazonaws.com"
  source_arn    = module.website_s3_bucket.arn
}


resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = module.website_s3_bucket.id

  lambda_function {
    lambda_function_arn = module.lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "formupload/form_"
    filter_suffix       = ".txt"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}


