variable "credentialFile" {
  type    = string
  default = "/home/lukas/.aws/credentials"
}

variable "region" {
  type    = string
  default = "eu-central-1"
}
variable "projectTagValue" {
  type    = string
  default = "ApplicationFormApp"
}

variable "bucketName" {
  type    = string
  default = "applicationform-kienast"
}

variable "tableName" {
  type    = string
  default = "applicationform-kienast-table"
}
